package pages.shop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Shop
{
    private WebDriver driver;
    private WebDriverWait wait;
    private Actions actions;

    public Shop(ChromeDriver driver) {
        PageFactory.initElements(driver, this);
         wait = new WebDriverWait(driver,30);
         actions = new Actions(driver);
    }

    @FindBy(xpath = "//span[@class='lang']")
    private WebElement englishButton;

    @FindBy(xpath ="//img[@src='https://eshop.vodafone.com.eg/eshopContent//product_images/original/applelogo.png']")
    private WebElement appleIconButton;

    public void clickOnEnglishButton() { englishButton.click(); }
    public void clickOnAppleIconButton() { //appleIconButton.click();

        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@src='https://eshop.vodafone.com.eg/eshopContent//product_images/original/applelogo.png']")));
        actions.moveToElement(appleIconButton).click().build().perform();

        }

}
