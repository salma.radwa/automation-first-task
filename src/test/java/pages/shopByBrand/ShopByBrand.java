package pages.shopByBrand;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShopByBrand {
    private WebDriver driver;
    private WebDriverWait wait;
    private Actions actions;


    public ShopByBrand(ChromeDriver driver) {
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 30);
        actions = new Actions(driver);


    }


    @FindBy(xpath = "(//span[contains(.,'Airpods Pro 2nd Generation')])[1]")
    private WebElement product;

    public void clickOnProduct() { //product.click();

       wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(.,'Airpods Pro 2nd Generation')])[1]")));
        actions.moveToElement(product).click().build().perform();


    }

}
