package pages.productDetails;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductDetails {
    private WebDriver driver;
    private WebDriverWait wait;
    private Actions actions;

    public ProductDetails(ChromeDriver driver) {

        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 30);
        actions = new Actions(driver);

    }


    @FindBy (xpath = "//button[@class='btn btn-red btn-block fontLightEnAr']")
    private WebElement addToBasketButton;

    public void clickOnAddToBasketButton() { //addToBasketButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn btn-red btn-block fontLightEnAr']")));
        actions.moveToElement(addToBasketButton).click().build().perform();

         }
}
