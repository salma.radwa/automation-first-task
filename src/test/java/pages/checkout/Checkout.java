package pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Checkout {

    private WebDriver driver;
    private WebDriverWait wait;
    private Actions actions;

    public Checkout(ChromeDriver driver) {

        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 30);
        actions = new Actions(driver);
    }


    @FindBy(xpath = "(//select[@class='btn dropdown-toggle checkout-dropdown border-radius_Style checkoutDeliveryFamilyFont ng-untouched ng-pristine ng-valid'])[1]")
    private WebElement cityDropDownMenu;

    @FindBy(xpath = "//option[@value='0']")
    private WebElement cairoOption;

    @FindBy(xpath = "(//select[@class='btn dropdown-toggle checkout-dropdown border-radius_Style checkoutDeliveryFamilyFont ng-untouched ng-pristine ng-valid'])[2]")
    private WebElement areaDropDownMenu;

    @FindBy(linkText = "Ain Shams")
    private WebElement ainShamsOption;

    @FindBy(xpath = "//div[@class='checkout-DelivaryOptionsInfo checkout-DelivaryToAddress open firstOpen']")
    private WebElement deliverToMyAddressButton;

    @FindBy(xpath = "//input[@placeholder=\"Street Name\"]")
    private WebElement streetNameTextBox;

    @FindBy(xpath = "//input[@formcontrolname=\"buildingNumber\"]")
    private WebElement buildingNumerTextBox;

    @FindBy(xpath = "//input[@formcontrolname=\"floorNumber\"]")
    private WebElement floorNumberTextBox;

    @FindBy(xpath = "//input[@formcontrolname=\"appartmentNumber\"]")
    private WebElement appartmentNumberTextBox;

    @FindBy(xpath = "//input[@placeholder=\"Landmark\"]")
    private WebElement landmarkTextBox;

    @FindBy(xpath = "//input[@placeholder=\"Address Name\"]")
    private WebElement addressNameTextBox;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement continueButton;

    @FindBy(xpath = "//*[text()=' Please enter a valid name ']")


    private WebElement errorMessage;

    public void clickOnCityDropDownMenu() {
        //cityDropDownMenu.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//select[@class='btn dropdown-toggle checkout-dropdown border-radius_Style checkoutDeliveryFamilyFont ng-untouched ng-pristine ng-valid'])[1]")));
        actions.moveToElement(cityDropDownMenu).click().build().perform();

    }

    public void clickOnCairoOption() {
        //cairoOption.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//option[@value='0']")));
        actions.moveToElement(cairoOption).click().build().perform();
    }

    public void clickOnAreaDropDownMenu() {
        //areaDropDownMenu.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//select[@class='btn dropdown-toggle checkout-dropdown border-radius_Style checkoutDeliveryFamilyFont ng-untouched ng-pristine ng-valid'])[2]")));
        actions.moveToElement(areaDropDownMenu).click().build().perform();
    }

    public void clickOnAinshamsOption() {
        //ainShamsOption.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Ain Shams")));
        actions.moveToElement(ainShamsOption).click().build().perform();
    }

    public void clickOnDeliverToMyAddressButton() {
        //deliverToMyAddressButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='checkout-DelivaryOptionsInfo checkout-DelivaryToAddress open firstOpen']")));
        actions.moveToElement(deliverToMyAddressButton).click().build().perform();
    }

    public void enterStreetName(String streetName) {
        //streetNameTextBox.sendKeys(streetName);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder=\"Street Name\"]")));
        actions.moveToElement(streetNameTextBox).click().build().perform();
    }

    public void enterBuildingNumber(String buildingNumber) {
        //buildingNumerTextBox.sendKeys(buildingNumber);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@formcontrolname=\"buildingNumber\"]")));
        actions.moveToElement(buildingNumerTextBox).click().build().perform();
    }

    public void enterFloorNumber(String floorNumber) {
        //floorNumberTextBox.sendKeys(floorNumber);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@formcontrolname=\"floorNumber\"]")));
        actions.moveToElement(floorNumberTextBox).click().build().perform();
    }

    public void enterAppartmentNumber(String appartmentNo) {
        //appartmentNumberTextBox.sendKeys(appartmentNo);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@formcontrolname=\"appartmentNumber\"]")));
        actions.moveToElement(appartmentNumberTextBox).click().build().perform();
    }

    public void enterLandmark(String landmark) {
        landmarkTextBox.sendKeys(landmark);
    }

    public void enterAddressName(String adressName) {
        addressNameTextBox.sendKeys(adressName);
    }

    public void clickOnContinueButton() {
        continueButton.click();
    }

    public String ErrorMessage() {
        return errorMessage.getText();
    }


}
