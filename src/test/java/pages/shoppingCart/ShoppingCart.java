package pages.shoppingCart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShoppingCart {
    private WebDriver driver;
    private WebDriverWait wait;
    private Actions actions;

    public ShoppingCart(ChromeDriver driver) {

        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver,30);
        actions = new Actions(driver);
    }


    @FindBy(xpath = "//button[contains(.,'Proceed to Checkout')]")
    private WebElement proceedToCheckoutButton;

    public void clickOnProceedToCheckoutButton() { //proceedToCheckoutButton.click();
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(.,'Proceed to Checkout')]")));
        actions.moveToElement(proceedToCheckoutButton).click().build().perform();
    }
}
