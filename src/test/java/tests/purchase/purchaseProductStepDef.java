package tests.purchase;
import io.cucumber.java.en.*;
import org.junit.Assert;
import pages.checkout.Checkout;
import pages.productDetails.ProductDetails;
import pages.shop.Shop;
import pages.shopByBrand.ShopByBrand;
import pages.shoppingCart.ShoppingCart;
import tests.BaseTest;

public class purchaseProductStepDef extends BaseTest {


    @Given("customer on the vodafone eshop page")
    public void customer_on_the_vodafone_eshop_page() {
        String URL = driver.getCurrentUrl();
        Assert.assertTrue(URL.contains("eshop"));
    }

    @When("click on english button")
    public void click_on_english_button() throws InterruptedException {
        Thread.sleep(10000);
        shop = new Shop(driver);
        shop.clickOnEnglishButton();


    }

    @When("click on apple brand icon in shop by brand section in eshop page")
    public void click_on_apple_brand_icon_in_shop_by_brand_section_in_eshop_page() throws InterruptedException {
        Thread.sleep(10000);
        shop = new Shop(driver);
        shop.clickOnAppleIconButton();

    }


    @When("click on desired product image in shop by brand page")
    public void click_on_desired_product_image_in_shop_by_brand_page() throws InterruptedException {
        Thread.sleep(5000);
        shopByBrand = new ShopByBrand(driver);
        shopByBrand.clickOnProduct();

    }

    @When("click on add to basket button in product details page")
    public void click_on_add_to_basket_button_in_product_details_page() throws InterruptedException {
        Thread.sleep(5000);
        productDetails = new ProductDetails(driver);
        productDetails.clickOnAddToBasketButton();
    }

    @When("click on proceed to checkout button in shopping cart page")
    public void click_on_proceed_to_checkout_button_in_shopping_cart_page() throws InterruptedException {
        Thread.sleep(5000);
        shoppingCart = new ShoppingCart(driver);
        shoppingCart.clickOnProceedToCheckoutButton();

    }

    @When("click on city in delivery options section in checkout page")
    public void click_on_city_in_delivery_options_section_in_checkout_page() throws InterruptedException {
        Thread.sleep(5000);
        checkout = new Checkout(driver);
        checkout.clickOnCityDropDownMenu();
        checkout.clickOnCairoOption();
    }

    @When("click on area drop down menu in delivery options section in checkout page")
    public void click_on_area_drop_down_menu_in_delivery_options_section_in_checkout_page() throws InterruptedException {
        Thread.sleep(2000);
        checkout.clickOnAreaDropDownMenu();
        checkout.clickOnAinshamsOption();
    }

    @When("click on deliver to my address button in delivery options section in checkout page")
    public void click_on_deliver_to_my_address_button_in_delivery_options_section_in_checkout_page() throws InterruptedException {
        Thread.sleep(2000);
        checkout.clickOnDeliverToMyAddressButton();
    }

    @When("enter street name {string} in street name field in delivery options section in checkout page")
    public void enter_street_name_in_street_name_field_in_delivery_options_section_in_checkout_page(String streetName) throws InterruptedException {
        Thread.sleep(5000);
        checkout.enterStreetName(streetName);
    }

    @When("enter building number {string} in building number field in delivery options section in checkout page")
    public void enter_building_number_in_building_number_field_in_delivery_options_section_in_checkout_page(String buildingNumber) throws InterruptedException {
        Thread.sleep(2000);
        checkout.enterBuildingNumber(buildingNumber);
    }

    @When("enter floor number {string} in floor number field in delivery options section in checkout page")
    public void enter_floor_number_in_floor_number_field_in_delivery_options_section_in_checkout_page(String floorNumber) throws InterruptedException {
        Thread.sleep(2000);
        checkout.enterFloorNumber(floorNumber);
    }

    @When("enter appartment number {string} in appartment number field in delivery options section in checkout page")
    public void enter_appartment_number_in_appartment_number_field_in_delivery_options_section_in_checkout_page(String appartmentNumber) throws InterruptedException {
        Thread.sleep(2000);
        checkout.enterAppartmentNumber(appartmentNumber);

    }

    @When("enter landmark {string} in landmark field in delivery options section in checkout page")
    public void enter_landmark_in_landmark_field_in_delivery_options_section_in_checkout_page(String landmark) throws InterruptedException {
        Thread.sleep(2000);
        checkout.enterLandmark(landmark);

    }

    @When("enter address name {string} in adress name field in delivery options section in checkout page")
    public void enter_address_name_in_adress_name_field_in_delivery_options_section_in_checkout_page(String addressName) throws InterruptedException {
        Thread.sleep(2000);
        checkout.enterAddressName(addressName);
    }

    @When("click on continue button in delivery options section in checkout page")
    public void click_on_continue_button_in_delivery_options_section_in_checkout_page() throws InterruptedException {
        Thread.sleep(2000);
        checkout.clickOnContinueButton();
    }

    @When("click on continue button in personal info section in checkout page")
    public void click_on_continue_button_in_personal_info_section_in_checkout_page() throws InterruptedException {
        Thread.sleep(5000);
        checkout.clickOnContinueButton();
    }

    @Then("validate that error message equale to {string}")
    public void validate_that_error_message_equale_to(String string) {
        org.testng.Assert.assertEquals(checkout.ErrorMessage(),"Please enter a valid name");
    }
}
