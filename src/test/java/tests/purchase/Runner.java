package tests.purchase;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.Test;
import cucumber.api.CucumberOptions;

@CucumberOptions(
        features="src/test/java/tests/purchase/purchaseProduct.feature",
        glue={"purchase"},
        monochrome = true

)


public class Runner extends AbstractTestNGCucumberTests {


}
