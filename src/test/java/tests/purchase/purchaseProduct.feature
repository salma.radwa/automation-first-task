Feature: purchase from vodafone store


  Scenario Outline: customer want to purchase a product from vodafone store
    Given customer on the vodafone eshop page
    When click on english button
    And click on apple brand icon in shop by brand section in eshop page
    And click on desired product image in shop by brand page
    And click on add to basket button in product details page
    And click on proceed to checkout button in shopping cart page
    And click on city in delivery options section in checkout page
    And click on area drop down menu in delivery options section in checkout page
    And click on deliver to my address button in delivery options section in checkout page
    And enter street name "<streetName>" in street name field in delivery options section in checkout page
    And enter building number "<buildingNumber>" in building number field in delivery options section in checkout page
    And enter floor number "<floorNumber>" in floor number field in delivery options section in checkout page
    And enter appartment number "<appartmentNo>" in appartment number field in delivery options section in checkout page
    And enter landmark "<landmark>" in landmark field in delivery options section in checkout page
    And enter address name "<adressName>" in adress name field in delivery options section in checkout page
    And click on continue button in delivery options section in checkout page
    And click on continue button in personal info section in checkout page
    Then validate that error message equale to "<expectedResult>"


    Examples:
      | streetName  | buildingNumber | floorNumber | appartmentNo | landmark    | adressName | expectedResult            |
      | Fouad Hamed | 10             | 4           | 9            | Zahraa Mall | Metro      | Please enter a valid name |

