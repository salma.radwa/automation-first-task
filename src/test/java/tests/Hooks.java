package tests;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


public class Hooks extends BaseTest {


    @Before
    public static void setUp() {


        String ChromePath = System.getProperty("user.dir") + "/resource/driver/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", ChromePath);
        driver = new ChromeDriver();
        driver.navigate().to("https://eshop.vodafone.com.eg/eshop");
        driver.manage().window().maximize();

    }


    @After
    public void CloseURL() {
        driver.close();
    }
}
