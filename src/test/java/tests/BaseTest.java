package tests;

import org.openqa.selenium.chrome.ChromeDriver;
import pages.checkout.Checkout;
import pages.productDetails.ProductDetails;
import pages.shop.Shop;
import pages.shopByBrand.ShopByBrand;
import pages.shoppingCart.ShoppingCart;

public class BaseTest {
    public static ChromeDriver driver;
    public static Shop shop;
    public static ShopByBrand shopByBrand;
    public static ProductDetails productDetails;
    public static ShoppingCart shoppingCart;
    public static Checkout checkout;

}
